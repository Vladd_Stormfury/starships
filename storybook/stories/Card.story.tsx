import React from "react";
import { ScrollView } from "react-native";
import { storiesOf } from "@storybook/react-native";

import { Starship } from "../../src/components/StarshipCard";

storiesOf("Card", module).add("default", () => (
  <ScrollView style={{ paddingTop: 40 }}>
    <Starship
      starship={{
        name: "My Starship",
        model: "My Model",
        manufacturer: "My Manufacturer",
        cost_in_credits: "100000",
      }}
    />
  </ScrollView>
));
