import React from "react";
import { View } from "react-native";
import { Button } from "react-native-paper";
import { storiesOf } from "@storybook/react-native";

storiesOf("Button", module)
  .add("default", () => (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Button mode="contained" onPress={() => console.log("Pressed")}>
        Press me
      </Button>
    </View>
  ))
  .add("button with icon", () => (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Button
        icon="camera"
        mode="contained"
        onPress={() => console.log("Pressed")}
      >
        Press me
      </Button>
    </View>
  ));
