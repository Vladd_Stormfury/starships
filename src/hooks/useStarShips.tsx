import { useQuery } from "react-query";

async function fetchData() {
  const result = await fetch("https://swapi.py4e.com/api/starships/");
  console.log(result);
  const json = await result.json();
  return json;
}

export const useStarships = () => {
  return useQuery(["starships"], fetchData);
};
