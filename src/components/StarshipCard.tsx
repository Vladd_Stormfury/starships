import React from "react";
import { TouchableHighlight } from "react-native";
import { Avatar, Card, Paragraph, Title } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

import { Routes } from "../navigation/Routes";

const LeftContent = (props: any[]) => (
  <Avatar.Icon {...props} icon="death-star" />
);

export const Starship = ({ starship }: any) => {
  const navigation = useNavigation();

  return (
    <TouchableHighlight
      onPress={() => {
        navigation.navigate(Routes.STARSHIP_DETAIL_SCREEN, {
          starship: starship,
        });
      }}
    >
      <Card>
        <Card.Title
          title={starship.name}
          subtitle={starship.model}
          left={LeftContent}
        />
        <Card.Content>
          <Paragraph>By {starship.manufacturer}</Paragraph>
        </Card.Content>
        <Card.Cover source={{ uri: "https://picsum.photos/700" }} />
        <Card.Content>
          <Title>Cost : {starship.cost_in_credits} credits</Title>
        </Card.Content>
      </Card>
    </TouchableHighlight>
  );
};
