import { NetworkConsumer, useIsConnected } from "react-native-offline";

import { Offline } from "./Offline";

export const ScreenContainer = (props: any) => {
  const isConnected = useIsConnected();

  return (
    <NetworkConsumer>
      {() => (isConnected ? props.children : <Offline />)}
    </NetworkConsumer>
  );
};
