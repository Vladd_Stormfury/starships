import { TextInput, Button } from "react-native-paper";
import { View, Text, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { Routes } from "../navigation/Routes";

export const LoginScreen = () => {
  const navigation = useNavigation();

  function navigateToTerms() {
    navigation.navigate(Routes.TERMS_SCREEN);
  }

  return (
    <View>
      <TextInput
        label="Email"
        onChangeText={(text) => {
          console.log(text);
        }}
      />
      <TextInput
        label="Password"
        onChangeText={(text) => {
          console.log(text);
        }}
      />
      <Button>Login</Button>
      <TouchableOpacity onPress={navigateToTerms}>
        <Text>{Routes.TERMS_SCREEN}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate(Routes.STARSHIP_FEED_SCREEN)}
      >
        <Text>{Routes.STARSHIP_FEED_SCREEN}</Text>
      </TouchableOpacity>
    </View>
  );
};
