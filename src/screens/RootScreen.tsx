// App.tsx

import React from "react";

import { Navigator } from "../navigation/Navigator";

export const RootScreen = () => {
  return <Navigator />;
};
