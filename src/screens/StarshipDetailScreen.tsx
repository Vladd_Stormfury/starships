import React, { useEffect, useState } from "react";
import { Text, ScrollView } from "react-native";
import { DataTable, Subheading } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

const optionsPerPage = [2, 3, 4];

export const StarshipDetailScreen = ({ route }: any) => {
  const { starship } = route.params;
  const [page, setPage] = useState<number>(0);
  const [itemsPerPage, setItemsPerPage] = useState(optionsPerPage[0]);
  const navigation = useNavigation();

  navigation.setOptions({
    title: starship.model,
  });

  useEffect(() => {
    setPage(0);
  }, [itemsPerPage]);

  return (
    <ScrollView>
      <Subheading>{starship.manufacturer}</Subheading>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>Dessert</DataTable.Title>
          <DataTable.Title numeric>Calories</DataTable.Title>
        </DataTable.Header>
        <DataTable.Row>
          <DataTable.Cell>Lenght</DataTable.Cell>
          <DataTable.Cell numeric>{starship.lenght}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Max Atmosphering Speed</DataTable.Cell>
          <DataTable.Cell numeric>
            {starship.max_atmosphering_speed}
          </DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Crew</DataTable.Cell>
          <DataTable.Cell numeric>{starship.crew}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Passengers</DataTable.Cell>
          <DataTable.Cell numeric>{starship.passengers}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Cargo Capacity</DataTable.Cell>
          <DataTable.Cell numeric>{starship.cargo_capacity}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Consumables</DataTable.Cell>
          <DataTable.Cell numeric>{starship.consumables}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>hyperdrive_rating</DataTable.Cell>
          <DataTable.Cell numeric>{starship.hyperdrive_rating}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>MGLT</DataTable.Cell>
          <DataTable.Cell numeric>{starship.MGLT}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>starship_class</DataTable.Cell>
          <DataTable.Cell numeric>{starship.starship_class}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>pilots</DataTable.Cell>
          <DataTable.Cell numeric>{starship.pilots}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Films</DataTable.Cell>
          <DataTable.Cell numeric>
            <Text>
              {starship.films.map((prop, key) => {
                return prop;
              })}
            </Text>
          </DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>created</DataTable.Cell>
          <DataTable.Cell numeric>{starship.created}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>edited</DataTable.Cell>
          <DataTable.Cell numeric>{starship.edited}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>url</DataTable.Cell>
          <DataTable.Cell numeric>{starship.url}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell>Cost</DataTable.Cell>
          <DataTable.Cell numeric>
            {starship.cost_in_credits} credits
          </DataTable.Cell>
        </DataTable.Row>
        <DataTable.Pagination
          page={page}
          numberOfPages={3}
          onPageChange={(myPage: number) => setPage(myPage)}
          label="1-2 of 6"
          optionsPerPage={optionsPerPage}
          itemsPerPage={itemsPerPage}
          setItemsPerPage={setItemsPerPage}
          showFastPagination
          optionsLabel={"Rows per page"}
        />
      </DataTable>
    </ScrollView>
  );
};
