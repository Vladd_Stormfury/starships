import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { NetworkProvider } from "react-native-offline";
import { QueryClient, QueryClientProvider } from "react-query";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { ScreenContainer } from "../components/ScreenContainer";
import { LoginScreen } from "../screens/LoginScreen";
import { TermsScreen } from "../screens/TermsScreen";
import { FeedScreen } from "../screens/FeedScreen";
import { StarshipDetailScreen } from "../screens/StarshipDetailScreen";

import { Routes } from "./Routes";

const Stack = createNativeStackNavigator();
const queryClient = new QueryClient();

export const Navigator = () => (
  <NetworkProvider>
    <QueryClientProvider client={queryClient}>
      <ScreenContainer>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name={Routes.LOGIN_SCREEN} component={LoginScreen} />
            <Stack.Screen name={Routes.TERMS_SCREEN} component={TermsScreen} />
            <Stack.Screen
              name={Routes.STARSHIP_FEED_SCREEN}
              component={FeedScreen}
            />
            <Stack.Screen
              name={Routes.STARSHIP_DETAIL_SCREEN}
              component={StarshipDetailScreen}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </ScreenContainer>
    </QueryClientProvider>
  </NetworkProvider>
);
