export enum Routes {
  LOGIN_SCREEN = "Login",
  TERMS_SCREEN = "Terms and Conditions",
  STARSHIP_FEED_SCREEN = "Starships",
  STARSHIP_DETAIL_SCREEN = "Starship",
}
